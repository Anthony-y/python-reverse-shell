![](http://i.imgur.com/78Om0Vg.png)

## Overview

This is my verison of BuckyRoberts' Python Reverse Shell. The differences between them are:

- At this time, my fork only supports ONE client at a time.
- The port for server.py is specified when it is run from the command line/terminal as shown below.
- The port AND IP address for the client are also specified as command line arguments.

To learn more about this program, watch the [YouTube Python Reverse Shell Tutorial Series](https://www.youtube.com/watch?v=1ObzpG_W_0o&list=PL6gx4Cwl9DGCbpkBEMiCaiu_3OL-_Bz_8&index=1)

Disclaimer: This reverse shell should only be used in the lawful, remote administration of authorized systems. Accessing a computer network without authorization or permission is illegal. 

## How to Use

To use this reverse shell, two scripts need to be running

* **server.py** - runs on a public server and waits for clients to connect
* **client.py** - connects to a remote server and then wait for commands

***

### Server

To set up server script, simply run **server.py** using Python 3.4

`python3 server.py <port>`

You will then enter an interactive prompt where you are able to send commands to the client remotely.

***

### Client

To setup a client, type:

`python3 client.py <ip> <port>`
